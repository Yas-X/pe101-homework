let num1 = prompt("Будь-ласка, введіть перше число");
while (!num1 || isNaN(num1)) {
    num1 = prompt("Будьте уважні! Будь-ласка, введіть перше число", num1);
};
let oper = prompt("Будь-ласка, введіть математичний оператор: +, -, *, /");
while (oper !== "+" && oper !=="-" && oper !== "*" && oper !== "/") {
    oper = prompt("Будьте уважні! Введіть математичний оператор: +, -, *, /", oper);
};
let num2 = prompt("Будь-ласка, введіть друге число");
while (!num2 || isNaN(num2)) {
    num2 = prompt("Будьте уважні! Будь-ласка, введіть друге число", num2);
};
function calc(num1, oper, num2) {
    switch (oper) {
        case "+":
            return +num1 + +num2;
            // break;
        case "-":
            return num1 - num2;
            // break;
        case "*":
            return num1 * num2;
            // break;
        case "/":
            return num1 / num2;
    }
};
alert(`${num1} ${oper} ${num2} = ${calc(num1, oper, num2)}`);