import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;
import autoprefixer from "gulp-autoprefixer";
import cleanCSS from "gulp-clean-css";
import imagemin from 'gulp-imagemin';

import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import clean from 'gulp-clean';

import browserSync from "browser-sync";
const bsServer = browserSync.create();

import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

function serve() {
    bsServer.init({
        server: {
            baseDir: "./",
            browser: "Chrome",
        },
    });
}

function styles() {
    return (
        src("./src/scss/*.scss")
            .pipe(sass().on("error", sass.logError))
            .pipe(
                autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
                    cascade: true,
                })
            )
            // .pipe(cleanCSS({ compatibility: "ie8" }))
            .pipe(concat('styles.min.css'))
            .pipe(dest("./dist/css/"))
            .pipe(bsServer.reload({ stream: true }))
    );
}

function scripts() {
    return src("./src/js/**/*.js")
        .pipe(concat('scripts.min.css'))
        .pipe(uglify())
        .pipe(dest("./dist/js/"))
        .pipe(bsServer.reload({ stream: true }));
}

function images() {
    return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
        .pipe(imagemin())
        .pipe(dest("./dist/img"))
        .pipe(bsServer.reload({ stream: true }));
}

function cleaner () {
    return src("./dist//**/*.*")
    .pipe(clean())
}

function watcher() {
    watch("./src/scss/**/*.scss", styles);
    watch("*.html").on("change", bsServer.reload);
    watch("./src/js/*.js").on("change", series(scripts, bsServer.reload));
    watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}").on(
        "change",
        series(images, bsServer.reload)
    );
}

export const dev = series(styles, scripts, images, parallel(serve, watcher));
export const build = series(styles, scripts, images, parallel(serve, watcher), cleaner);