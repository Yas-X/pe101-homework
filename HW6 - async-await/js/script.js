const root = document.getElementById("root");
const findBtn = document.querySelector(".findIP");

function getIPAddress(urlIP, urlFiz) {
    findBtn.addEventListener("click", async () => {
        await fetch(urlIP, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then((data) => {
                const pIP = document.createElement("p");
                pIP.classList.add("addressIP");
                pIP.textContent = "Your IP address: " + data.ip;
                root.append(pIP);
            })
            .catch(err => console.log(err))

        fetch((urlFiz + "json/" + (document.querySelector(".addressIP").textContent.split("Your IP address: ")[1])), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then(({timezone, country, region, city}) => {

                //console.log((document.querySelector(".addressIP").textContent.split("Your IP address: ")[1]));
                const address = document.createElement("ul");
                const continent = document.createElement("li");
                continent.textContent = "Continent: " + timezone;
                address.append(continent);
                const countryIP = document.createElement("li");
                countryIP.textContent = "Country: " + country;
                address.append(countryIP);
                const regionIP = document.createElement("li");
                regionIP.textContent = "Region: " + region;
                address.append(regionIP);
                const cityIP = document.createElement("li");
                cityIP.textContent = "City: " + city;
                address.append(cityIP);
                //console.log(timezone)
                root.append(address);
            })
            .catch(err => console.log(err))
    });
}

getIPAddress("https://api.ipify.org/?format=json", "http://ip-api.com/")