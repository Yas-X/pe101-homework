const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const root = document.getElementById("root");

class CheckBooks {
    constructor(books) {
        this.booksArray = [...books]
    };

    render(root) {
        const bookArray = document.createElement("ul");
        root.append(bookArray);

        this.booksArray.forEach((bookObj, index) => {

            try {
                const author = bookObj.author;
                const name = bookObj.name;
                const price = bookObj.price;
                if (!author || !name || !price) {
                    throw e;
                }
                const book = document.createElement("li");
                book.innerText = `Author: ${author}; Book name: "${name}"; Book price: ${price} UAH`;
                bookArray.append(book);

            } catch (e) {
                console.log(`Some of property (author, name, price) is not defind at object ${index}`);
            };

        });
    }
};

const checkBook = new CheckBooks(books);
checkBook.render(root);