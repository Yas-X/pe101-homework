const buttons = document.getElementsByClassName("btn");
document.addEventListener('keydown', function (event) {
    const result = Array.from(buttons).filter(button => button.innerText === (event.code).slice(3) || button.innerText === event.code);
    const other = Array.from(buttons).filter(button => button.innerText !== (event.code).slice(3) || button.innerText !== event.code);
    other.forEach(button => button.style.backgroundColor = "#000000");
    result[0].style.backgroundColor = "blue";
});