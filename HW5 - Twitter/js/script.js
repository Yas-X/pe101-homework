const root = document.getElementById("root");
const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";

const postsList = document.createElement("ul");

class Card {
    constructor(postsList, post, user) {
        this.postsList = postsList;
        this.user = user;
        this.post = post
    }

    renderCard() {
        const postItem = document.createElement("li");
        const userName = document.createElement("h3");
        userName.classList.add("userName");
        userName.textContent = this.user.name;
        const userNikName = document.createElement("span");
        userNikName.classList.add("userNikName");
        userNikName.textContent = `@${this.user.username}`;
        const userMail = document.createElement("span");
        userMail.classList.add("userEmail");
        userMail.textContent = this.user.email;
        userName.append(userNikName, userMail);

        const postTitle = document.createElement("h4");
        postTitle.classList.add("postTitle");
        postTitle.innerHTML = `<span>&#127988;</span>` + this.post.title;
        const postText = document.createElement("p");
        postText.classList.add("postText");
        postText.textContent = this.post.body;
        const closeBtn = document.createElement("button");
        closeBtn.classList.add("closeBtn");
        closeBtn.innerText = "X";
        postItem.append(userName, closeBtn, postTitle, postText);
        this.postsList.append(postItem)
        const idCard = this.post.id;
        this.removePost(postItem, idCard)
    }

    removePost(card, id) {
        card.addEventListener("click", (event) => {
            let targetPost = event.target;
            if (targetPost.closest(".closeBtn")) {
                fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                    method: 'DELETE',
                })
                    .then(response => console.log(response))
                    .then(() => targetPost.closest("li").remove())
                    .catch(e => console.log(e))
            }
        })
    }
}

root.append(postsList);

let users = [];
const userRequest = new Promise((resolve, reject) => {
    fetch(usersUrl, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((resolve) => resolve.json())
        .then(data => {
            //console.log(data);
            resolve(users = data)
        })
        .catch((err) => reject(err));
})

let posts = [];
const postRequest = new Promise((resolve, reject) => {
    fetch(postsUrl, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((resolve) => resolve.json())
        .then(data => {
            resolve(posts = data)
        })
        .catch((err) => reject(err));
})

Promise.all([userRequest, postRequest])
    .then((values) => {
        values[1].forEach(post => {
            const user = values[0][post.userId - 1];
            const card = new Card(postsList, post, user);
            card.renderCard()
        })
    })
    .catch(e => console.log(e))