class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(value) {
        if (value.length > 2) {
            this._name = value;
        } else {
            this._name = "Anonimous";
        }
    }
    get name() {
        return this._name;
    }

    set age(value) {
        if (value >= 18) {
            this._age = value;
        } else {
            this._age = "Employee is too yuong!!!";
        }
    }
    get age() {
        return this._age;
    }

    set salary(value) {
        if (value >= 1000) {
            this._salary = value;
        } else {
            this._salary = "Employee has low qualified. Salary too low!!!";
        }
    }
    get salary() {
        return this._salary;
    }
};

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        if (value >= 1000) {
            this._salary = 3 * super.salary;
        } else {
            this._salary = "Employee has low qualified. Salary too low!!!";
        }
    }
    get salary() {
        return this._salary;
    }
};

const programmer = new Programmer("John", 18, 1000, "spanish");
console.log(programmer);
const programmer1 = new Programmer("Sandra", 18, 1500, "italian");
console.log(programmer1);
const programmer2 = new Programmer("Kevin", 13, 300, "english");
console.log(programmer2);