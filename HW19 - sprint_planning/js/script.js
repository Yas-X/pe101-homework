function planningDeadLine(speedWorkers, quantityTasks, date) {
    const workersDay = speedWorkers.reduce((a, b) => a + b);
    const task = quantityTasks.reduce((a, b) => a + b);
    let tasksDays = task / workersDay;
    console.log(`Необхідна кількість робочих днів для виконання завдання: ${tasksDays.toFixed(2)}`);

    const start = new Date();
    start.setHours(9, 0, 0, 0);
    console.log(`Дата початку виконання завдання: ${start}`);

    const end = new Date(Date.parse(date));
    end.setHours(18, 0, 0, 0);
    console.log(`Дата закінчення виконання завдання: ${end}`);

    for (let i = start; i < end; i.setDate(i.getDate() + 1)) {
        if (i.getDay() === 0 || i.getDay() === 6) {
            continue;
        } else {
            tasksDays--;
        };
    };

    if (tasksDays <= 0) {
        return console.log(`Усі завдання будуть успішно виконані за ${Math.floor(Math.abs(tasksDays))} днів до настання дедлайну!`);
    } else {
        return console.log(`Команді розробників доведеться витратити додатково ${(tasksDays * 8).toFixed(2)} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    };
}
let speedWorkers = [5, 8, 4, 5];
let quantityTask = [14, 28, 12, 23, 35, 45, 34];
const deadLineDate = "2022-11-30";

planningDeadLine(speedWorkers, quantityTask, deadLineDate);