const URL = "https://ajax.test-danit.com/api/swapi/films";
const root = document.getElementById("root");
// const spener = document.createElement("div");
// spener.classList = "lds-ring";
// spener.innerHTML = "<div></div><div></div><div></div><div></div>";

class StarWars {
	constructor(root) {
		this.root = root;
	}
	renderEntity(data) {
		const entityList = document.createElement("ul");
		data.forEach(({ episodeId, name, openingCrawl, characters }) => {
			const infoEntity = document.createElement("li");
			infoEntity.innerHTML = `<p>Episode #: ${episodeId}</p><p> Film name: ${name}</p><p> Short description: ${openingCrawl}</p>`;
			entityList.append(infoEntity);
			const promiseListPerson = characters.map((item) => {
				return fetch(item)
					.then((response) => {
						return response.json();
					})
			});


			Promise.all(promiseListPerson)
				.then((data) => {
					const listPerson = document.createElement("p");
					listPerson.innerText = `Persons of movie:`;
					data.forEach(({ name }) => {
						listPerson.innerText += ` ${name},`;
					})
					infoEntity.append(listPerson);
				})
				.catch((e) => console.log(e));
		})
		this.root.append(entityList)
	};
}

class Requests {
	constructor(url) {
		this.url = url;
	}
	get() {
		return fetch(this.url).then((response) => {
			return response.json();
		});
	}
}


const request = new Requests(URL);
const starWars = new StarWars(root);

request.get()
	.then((data) => {
		starWars.renderEntity(data)
	})
	.catch((e) => {
		console.log(e);
	});