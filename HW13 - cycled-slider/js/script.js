const picturies = Array.from(document.getElementsByClassName("image-to-show"));
picturies.forEach(picture => picture.style.display = "none");
let index = 0;
let interval;
let timerId = setTimeout(changePicture, 0);

const stopBtn = document.createElement("button");
stopBtn.innerText = "Припинити";
stopBtn.style.margin = "10px";
document.body.append(stopBtn);

const startBtn = document.createElement("button");
startBtn.innerText = "Відновити показ";
startBtn.style.margin = "10px";

function changePicture() {
    if (index === 0) {
        fadeOut(picturies[picturies.length - 1], 500);
    } else {
        fadeOut(picturies[index - 1], 500)
    };
    fadeIn(picturies[index], 500);

    if (index === picturies.length - 1) {
        index = 0;
    } else { ++index }
    return timerId = setTimeout(changePicture, 3000);
};

function fadeIn(el, timeOut) {
    timer();
    el.style.opacity = 0;
    el.style.display = "";
    el.style.transition = `opacity ${timeOut}ms`;
    setTimeout(() => {
        el.style.opacity = 1;
    }, 5);
};

function fadeOut(el, timeOut) {
    el.style.opacity = 1;
    el.style.transition = `opacity ${timeOut}ms`;
    el.style.opacity = 0;
    el.style.display = "none";
    clearInterval(interval);
};

stopBtn.addEventListener("click", () => {
    clearTimeout(timerId);
    clearInterval(interval);
    document.body.append(startBtn);
});

startBtn.addEventListener("click", () => {
    timerId = setTimeout(changePicture, 0);
    startBtn.remove();
});

function timer() {
    const startTime = Date.now();

    interval = setInterval(function timer() {
        let elapsedTime = Date.now() - startTime;
        document.getElementById("timer").innerHTML = (elapsedTime / 1000).toFixed(3);
    }, 1);
};