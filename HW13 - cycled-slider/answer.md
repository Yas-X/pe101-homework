Теоретичні питання
Опишіть своїми словами різницю між функціями setTimeout() і setInterval()` - Функція setTimeout() запускає функцію (чи код) через визначений проміжок часу. Функція setInterval() запускаю функцію (чи код) циклісно через визначений проміжок часу

Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому? - Функція спрацює миттєво після виконанання коду скрипту

Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен? -  Функція посилається на зовнішнє  середовище. Вони можуть зайняти набагато більше пам’яті, ніж сама функція. Тому, коли нам більше не потрібна запланована функція, краще її скасувати.