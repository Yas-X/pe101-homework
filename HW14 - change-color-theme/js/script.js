document.querySelector(".theme").addEventListener("click", () => {
    if (localStorage.getItem("theme") === "dark") {
        localStorage.removeItem("theme");
    } else {
        localStorage.setItem("theme", "dark")
    }
    addDarkTheme();
});

function addDarkTheme() {
    if (localStorage.getItem("theme") === "dark") {
        document.querySelector("html").classList.add("dark");
    } else {
        document.querySelector("html").classList.remove("dark");
    }
};

addDarkTheme ();