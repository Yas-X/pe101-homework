function createNewUser() {
    let firstName = prompt("Enter your name, please:", "Yaroslav");
    let lastName = prompt("Enter your surname, please:", "Havrylenko");
    const birthday = prompt("Enter your day of birthday,please:", "dd.mm.yyyy");
    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        birthday,
        getLogin() {
            return (this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase());
        },
        getAge() {
            const date = new Date;
            const birthdayArr = this.birthday.split(".");
            const dateBirthday = new Date(birthdayArr[2], (birthdayArr[1] - 1), birthdayArr[0]);
            if (date.getMonth() > dateBirthday.getMonth()) {
                return date.getFullYear() - dateBirthday.getFullYear();
            } else {
                if (date.getDate() > dateBirthday.getDate()) {
                    return date.getFullYear() - dateBirthday.getFullYear();
                } else return date.getFullYear() - dateBirthday.getFullYear() - 1;
            }
        },
        getPassword() {
            return (this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4));
        },
        set firstName(value) {
            this._firstName = value;
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(value) {
            this._lastName = value;
        },
        get lastName() {
            return this._lastName;
        }
    };
    return newUser;
}
let user = createNewUser();
console.log(`Login: ${user.getLogin()}`);
console.log(`Password: ${user.getPassword()}`);
console.log(`Age: ${user.getAge()}`);