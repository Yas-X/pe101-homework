function createList(array, parent = document.body) {
    const ol = document.createElement("ol");
    parent.append(ol);

    array.forEach(elem => {
        const li = document.createElement("li");
        ol.append(li);
        if (!Array.isArray(elem)) {
            li.innerText = elem;
        } else { createList(elem, li) }
    })
    return;
}

createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

let showTime = 3000;
const timer = document.createElement('div');
timer.innerText = `Timer: ${showTime / 1000} c`;
document.body.prepend(timer);

setInterval(() => {
    timer.innerText = `Timer: ${(showTime -= 1000) / 1000} c`;
    if (showTime < 0) {
        document.body.innerHTML = "";
        clearInterval()
    }
}, 1000);