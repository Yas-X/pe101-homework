// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let pargrafs = document.getElementsByTagName('P');
console.log(pargrafs);
for (const pargraf of pargrafs) {
    pargraf.style.backgroundColor = "#ff0000";
};

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
console.log(document.getElementById("optionsList"));
console.log(optionsList.parentElement);
console.log(optionsList.childNodes);
for (const node of optionsList.childNodes) {
    console.log(`Назва ноди: ${node.nodeName}. Тип ноди: ${node.nodeType}`);
};

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
document.getElementById("testParagraph").innerText = "This is a paragraph";

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = Array. from(document.querySelector(".main-header").children);
console.log(mainHeader);
for (const child of mainHeader) {
    console.log(`Child from "main-header": ${child}`);
    child.classList.add("nav-item");
};

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = Array.from(document.getElementsByClassName("section-title"));
sectionTitle.forEach(removeClass);
function removeClass(elem) {
    elem.classList.remove("section-title");
};
console.log(sectionTitle);