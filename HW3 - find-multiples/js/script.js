alert("Обов'язкове завдання");
let num;
do {
    num = prompt("Please enter number:", num);
} while (!(!isNaN(num) && num && (Number(num) === parseInt(num))));
for (let i = 1; i <= num; i++) {
    if (num < 5) {
        console.log("Sorry, no numbers");
        break;
    } else if (i % 5 === 0) console.log(i);
}


alert("Необов'язкове завдання підвищеної складності");
let m = prompt("Please enter first number:");
let n = prompt("Please enter second number:");
while (!(!isNaN(m) && m && !isNaN(n) && n && (m < n))) {
    alert("Please enter correct numbers!")
    m = prompt("Please enter first number:", m);
    n = prompt("Please enter second number:", n);
};
nextLoop: 
for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {
        if (i % j == 0) {
            continue nextLoop;
        }
    }
    console.log(`Natural number is ${i}`);

}
console.log("correct");