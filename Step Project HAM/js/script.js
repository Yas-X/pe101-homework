const serviceMemu = document.querySelector(".service-menu");
const serviceContent = document.querySelector(".service-content").children;

function activator(params) {
    Array.from(serviceContent).forEach(item => {
        if (item.dataset.title === params) {
            return item.style.display = "";
        } else {
            return item.style.display = "none";
        }
    })
}

function switcher() {
    serviceMemu.addEventListener("mouseup", (event) => {
        document.querySelector(".active").classList.remove("active");
        event.target.classList.add("active");
        return activator(event.target.textContent)
    })

};
activator(document.body.querySelector(".active").textContent);
switcher();

const workMenu = document.querySelector(".work-menu");
const imageBlock = document.querySelector(".image-block");
const imageBlockItems = Array.from(document.querySelectorAll(".image-block-item"));
const loadBtn = document.querySelector(".load-button");
const visibleImageBlockItems = [...imageBlockItems];

const amazingMenuItems = ["Graphic Design", "Web Design", "Landing Pages", "Wordpress"]
const graphicDesign = ["graphic-design1", "graphic-design2", "graphic-design3", "graphic-design4", "graphic-design5", "graphic-design6", "graphic-design7", "graphic-design8", "graphic-design9", "graphic-design10", "graphic-design11", "graphic-design12"];
const webDesign = ["web-design1", "web-design2", "web-design3", "web-design4", "web-design5", "web-design6", "web-design7"];
const landingPages = ["landing-page1", "landing-page2", "landing-page3", "landing-page4", "landing-page5", "landing-page6", "landing-page7"];
const wordpress = ["wordpress1", "wordpress2", "wordpress3", "wordpress4", "wordpress5", "wordpress6", "wordpress7", "wordpress8", "wordpress9", "wordpress10"];

function partOfPictures() {
    const arrNewPic = [];
    do {
        let newPicBlocK = document.createElement("li");
        newPicBlocK.className = "image-block-item";
        let newPic = new Image;
        switch (arrayRandElement(amazingMenuItems)) {
            case "Graphic Design":
                newPic.src = `./image/graphic design/${arrayRandElement(graphicDesign)}.jpg`;
                newPic.alt = "Graphic Design";
                newPic.className = "main-image"
                newPicBlocK.innerHTML = '<div class="imgBackground"><div class="image-back-logo"><div class="chain"><img class="chain-image" src="./image/logo/chain.svg" alt="chain"></div><div class="round"><div class="square"></div></div></div><p class="background-title">CREATIVE DESIGN</p><P class="background-text">Graphic Design</P></div>';
                newPicBlocK.append(newPic);
                break;
            case "Web Design":
                newPic.src = `./image/web design/${arrayRandElement(webDesign)}.jpg`;
                newPic.alt = "Web Design";
                newPic.className = "main-image"
                newPicBlocK.innerHTML = '<div class="imgBackground"><div class="image-back-logo"><div class="chain"><img class="chain-image" src="./image/logo/chain.svg" alt="chain"></div><div class="round"><div class="square"></div></div></div><p class="background-title">CREATIVE DESIGN</p><P class="background-text">Web Design</P></div>';
                newPicBlocK.append(newPic);
                break;
            case "Landing Pages":
                newPic.src = `./image/landing page/${arrayRandElement(landingPages)}.jpg`
                newPic.alt = "Landing Pages";
                newPic.className = "main-image"
                newPicBlocK.innerHTML = '<div class="imgBackground"><div class="image-back-logo"><div class="chain"><img class="chain-image" src="./image/logo/chain.svg" alt="chain"></div><div class="round"><div class="square"></div></div></div><p class="background-title">CREATIVE DESIGN</p><P class="background-text">Landing Pages</P></div>';
                newPicBlocK.append(newPic);
                break;
            case "Wordpress":
                newPic.src = `./image/wordpress/${arrayRandElement(wordpress)}.jpg`;
                newPic.alt = "Wordpress";
                newPic.className = "main-image"
                newPicBlocK.innerHTML = '<div class="imgBackground"><div class="image-back-logo"><div class="chain"><img class="chain-image" src="./image/logo/chain.svg" alt="chain"></div><div class="round"><div class="square"></div></div></div><p class="background-title">CREATIVE DESIGN</p><P class="background-text">Wordpress</P></div>';
                newPicBlocK.append(newPic);
                break;
        }
        arrNewPic.push(newPicBlocK)
    } while (arrNewPic.length < 12);
    return arrNewPic
}

arrayRandElement(amazingMenuItems);

function arrayRandElement(arr) {
    let rand = Math.floor(Math.random() * arr.length);
    return arr[rand];
}

function addPictures() {
    let countclick = 0;
    loadBtn.addEventListener("click", () => {
        loadBtn.style.display = "none";
        document.body.querySelector(".our-amazing-work").querySelector(".clock-loader").style.display = "";
        setTimeout(() => {
            document.body.querySelector(".our-amazing-work").querySelector(".clock-loader").style.display = "none";
            loadBtn.style.display = "";
            document.body.querySelector(".our-amazing-work").querySelector(".active").classList.remove("active");
            document.body.querySelector(".our-amazing-work").querySelector(".work-item").classList.add("active");
            ++countclick;
            if (countclick === 1) {
                partOfPictures().forEach(element => {
                    document.body.querySelector(".our-amazing-work").querySelector(".image-block").append(element);
                    visibleImageBlockItems.push(element);
                });
            } else {
                partOfPictures().forEach(element => {
                    document.body.querySelector(".our-amazing-work").querySelector(".image-block").append(element);
                    visibleImageBlockItems.push(element);
                });
                loadBtn.remove();
            }
        }, 2000);
    })
}
addPictures();

function workMenuActivator(title) {
    visibleImageBlockItems.forEach(item => {
        if (title === "All") {
            return item.style.display = ""
        } else if (item.querySelector(".main-image").getAttribute("alt") === title) {
            return item.style.display = "";
        } else {
            return item.style.display = "none";
        }
    });
}

function choosePicture() {
    workMenu.addEventListener("mouseup", event => {
        document.body.querySelector(".our-amazing-work").querySelector(".work-menu").querySelector(".active").classList.remove("active");
        event.target.classList.add("active");
        return workMenuActivator(event.target.textContent)
    })
}

workMenuActivator(document.body.querySelector(".our-amazing-work").querySelector(".active").textContent);
choosePicture()

let selectedImg;
imageBlock.addEventListener("mouseover", event => {
    if (event.target.className != 'main-image') return;
    showImageBackground(event.target);
});
imageBlock.addEventListener("mouseout", () => selectedImg.style.opacity = "1");

function showImageBackground(target) {
    if (selectedImg) {
        selectedImg.style.opacity = "1";
    }
    selectedImg = target;
    target.style.opacity = "0";
};

// CAROUSEL
const carousel = document.body.querySelector(".carousel");

let i = 1;
for (let li of carousel.querySelectorAll('li')) {
    li.style.position = 'relative';
    li.insertAdjacentHTML('beforeend', `<span style="position:absolute;left:0;top:0">${i}</span>`);
    i++;
}

let width = 70;
let count = 3;

let list = carousel.querySelector('ul');
let listElems = carousel.querySelectorAll('li');

let position = 0;

carousel.querySelector('.back').onclick = function () {
    position += width * count;
    position = Math.min(position, 0)
    list.style.marginLeft = position + 'px';
};

carousel.querySelector('.forward').onclick = function () {
    position -= width * count;
    position = Math.max(position, -width * (listElems.length - count));
    list.style.marginLeft = position + 'px';
};

const itemsInfo = document.body.querySelector(".carousel-info").querySelectorAll(".item-info");
const photoList = Array.from(document.body.querySelector(".carousel-item").getElementsByClassName("show-person-small"));
let showPerson = document.body.querySelector(".show-person")
list.addEventListener("click", (event) => {
    const activeInfo = document.body.querySelector(".carousel-info").querySelector(".active");
    showPerson.style.opacity = 0.3;
    setTimeout(() => {
        showPerson.setAttribute("src", `${event.target.src}`);
        showPerson.style.opacity = 1;
    }, 700);
    activeInfo.classList.remove("active");
    photoList.forEach((photo, index) => {
        if (photo === event.target) itemsInfo[index].classList.add("active");
    });
});