const icons = document.getElementsByTagName("i");
const btn = document.querySelector(".btn");

function changeIcon() {
    Array.from(icons).forEach(element => {
        element.addEventListener("click", (event) => {
            event.target.classList.toggle("fa-eye");
            event.target.classList.toggle("fa-eye-slash");
            if (event.target.classList.contains("fa-eye")) {
                event.target.previousElementSibling.type = "";
            } else { event.target.previousElementSibling.type = "password" }
        })
    });
}

function confirmPass(pass1, pass2) {
    if (pass1 === pass2 && pass1 !== "" && pass2 !== "") {
        console.log(`pass1: ${pass1}`);
        console.log(`pass2: ${pass2}`);

        if (document.querySelector(".note")) {
            document.querySelector(".note").remove()
        };
        delayedAlert();
    } else {
        if (!document.querySelector(".note")) {
            const note = document.createElement('p');
            note.style.color = "red";
            note.classList.add("note");
            note.textContent = "Потрібно ввести однакові значення";
            const inputs = document.getElementsByClassName("input-wrapper");
            inputs[inputs.length - 1].after(note);
        }
    }
}

function getPass() {
    btn.addEventListener("click", (event) => {
        event.preventDefault();
        const pass1 = document.querySelector(".input1").value;
        const pass2 = document.querySelector(".input2").value;
        return confirmPass(pass1, pass2);
    })
}

function delayedAlert() {
    timeoutID = window.setTimeout(slowAlert, 1000);
}
function slowAlert() {
    alert("You are welcome");
}

getPass();
changeIcon();