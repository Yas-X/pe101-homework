function createNewUser() {
    let firstName = prompt("Enter your name, please:", "Yaroslav");
    let lastName = prompt("Enter your surname, please:", "Havrylenko");
    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        getLogin() {
            return (this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase())
        },
        set firstName(value) {
            this._firstName = value;
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(value) {
            this._lastName = value;
        },
        get lastName() {
            return this._lastName;
        }
    };
    return newUser;
}
let user = createNewUser();
console.log(`Login: ${user.getLogin()}`);