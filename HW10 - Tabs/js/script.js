const tabs = document.querySelector(".tabs");
const tabsContent = document.querySelector(".tabs-content").children;

function activator(params) {
    Array.from(tabsContent).forEach(item => {
        if (item.dataset.title === params) {
           return item.style.display = "";
        } else {
           return item.style.display = "none";
        }
    })
}

function switcher() {
        tabs.addEventListener("mouseup", (event) => {
            document.querySelector(".active").classList.remove("active");
            event.target.classList.add("active");
            return activator(event.target.textContent)
        })
    
};

activator(document.querySelector(".active").textContent);
switcher();